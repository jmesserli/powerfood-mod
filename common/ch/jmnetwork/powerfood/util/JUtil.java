package ch.jmnetwork.powerfood.util;

import ch.jmnetwork.powerfood.lib.Reference;

public class JUtil {
	String modName = "";

	/**
	 * @param mname
	 *            Name of the Mod
	 */
	public JUtil(String mname) {
		modName = mname;
	}

	public int getShiftedIndex(int i) {
		return i + 256;
	}

	public void print(String stringToPrint) {
		System.out.println("[" + modName + "] " + stringToPrint);
	}

	public String getTextureString(String itemTextureFile) {
		return Reference.MOD_ID + ":" + itemTextureFile;
	}
}
