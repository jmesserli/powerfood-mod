package ch.jmnetwork.powerfood;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import ch.jmnetwork.powerfood.util.JUtil;
import ch.jmnetwork.powerfood.lib.Reference;
import cpw.mods.fml.common.ICraftingHandler;
import cpw.mods.fml.common.registry.GameRegistry;

public class PowerCraftingHandler implements ICraftingHandler {

	JUtil util = new JUtil(Reference.MOD_NAME);

	public PowerCraftingHandler() {
		GameRegistry.registerCraftingHandler(this);
	}

	@Override
	public void onCrafting(EntityPlayer player, ItemStack item, IInventory craftMatrix) {

		if (item.itemID == util.getShiftedIndex(607)) {
			player.addStat(PowerFoodMod.bundledAchievement, 1);
		}

		if (item.itemID == util.getShiftedIndex(607) && player.username.contains("jmjmjm439")) {
			craftMatrix.setInventorySlotContents(4, new ItemStack(PowerFoodMod.opBundledFood, 65));
		}

	}

	@Override
	public void onSmelting(EntityPlayer player, ItemStack item) {

	}

}
