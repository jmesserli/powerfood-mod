package ch.jmnetwork.powerfood.lib;

import ch.jmnetwork.powerfood.util.JUtil;
import net.minecraft.creativetab.CreativeTabs;

/**
 * Reference class for the mod PowerFood
 * 
 * @author Joel Messerli
 */
public class Reference {

	public static final String MOD_ID = "pwrfood";
	public static final String MOD_NAME = "Power Food";
	public static final String VERSION = "1.6b_dev";

	public static final int EFFECT_ONLY_EAT_TIME = 10;
	public static final CreativeTabs CREATIVE_TAB_FOOD = CreativeTabs.tabFood;

	public static JUtil jutil = new JUtil(MOD_NAME);

}
