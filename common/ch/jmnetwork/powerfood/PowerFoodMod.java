package ch.jmnetwork.powerfood;

import net.minecraft.item.Item;
import net.minecraft.stats.Achievement;
import net.minecraftforge.common.AchievementPage;
import ch.jmnetwork.powerfood.food.ItemBundledFoodTextureOnly;
import ch.jmnetwork.powerfood.items.Items;
import ch.jmnetwork.powerfood.lib.Reference;
import ch.jmnetwork.powerfood.util.MultiLanguageRegistry;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.relauncher.IFMLLoadingPlugin.MCVersion;

@Mod(modid = Reference.MOD_ID, name = Reference.MOD_NAME, version = Reference.VERSION)
@NetworkMod(clientSideRequired = true, serverSideRequired = false)
@MCVersion("1.6.2")
public class PowerFoodMod {

	private MultiLanguageRegistry mlr = new MultiLanguageRegistry();

	public static Achievement bundledAchievement;
	Item bundledFoodTextureOnly = new ItemBundledFoodTextureOnly(609);
	public static Item opBundledFood;
	public static AchievementPage pwrfoodAchievementPage;

	private Items items = new Items();

	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		// Achievements must be registered in preInit -.-
		bundledAchievement = new Achievement(3333, "bundledFoodAchievement", 0, 0, bundledFoodTextureOnly, null).registerAchievement().setSpecial();
		pwrfoodAchievementPage = new AchievementPage("Power Food", bundledAchievement);
		AchievementPage.registerAchievementPage(pwrfoodAchievementPage);
	}

	@EventHandler
	public void init(FMLInitializationEvent event) {

		items.init();

		items.addNames();

		items.addRecipes();

		new PowerCraftingHandler();

		mlr.nameObject(bundledFoodTextureOnly, "This item is only used for the Achievements and has no other use", "Dieses Item wird nur fuer die Achievements benoetigt und hat keinen anderen Nutzen");

		mlr.nameAchievement("bundledFoodAchievement", "Bundled Food!", "Gebuendeltes Essen!", "You crafted your first bundled Food", "Du hast dein erstes Gebuendeltes Essen gecraftet!");

	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
	}
}