package ch.jmnetwork.powerfood.items;

import ch.jmnetwork.powerfood.PowerFoodMod;
import ch.jmnetwork.powerfood.food.ItemBasicFood;
import ch.jmnetwork.powerfood.food.ItemBasicFoodCooked;
import ch.jmnetwork.powerfood.food.ItemBattleFood;
import ch.jmnetwork.powerfood.food.ItemBundledFood;
import ch.jmnetwork.powerfood.food.ItemClimberFood;
import ch.jmnetwork.powerfood.food.ItemCookedZombieFlesh;
import ch.jmnetwork.powerfood.food.ItemMinerFood;
import ch.jmnetwork.powerfood.food.ItemOPBundledFood;
import ch.jmnetwork.powerfood.food.ItemTravelerFood;
import ch.jmnetwork.powerfood.lib.Reference;
import ch.jmnetwork.powerfood.util.MultiLanguageRegistry;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.FurnaceRecipes;

public class Items {
	MultiLanguageRegistry mlr = new MultiLanguageRegistry();

	public Items() {
	}

	Item basicFood;
	Item basicFoodCooked;
	Item battleFood;
	Item bundledFood;
	Item travelerFood;
	Item climberFood;
	Item minerFood;
	Item cookedFlesh;

	public void init() {
		basicFood = new ItemBasicFood(ItemReference.BASICFOOD_DEFAULT_ID, 2, 0.1F, true, 17, 30, 1, 0.8F);
		basicFoodCooked = new ItemBasicFoodCooked(ItemReference.BASICFOOD_COOKED_DEFAULT_ID, 10, 1.0F, true, 1, 15, 3, 1.0F);
		battleFood = new ItemBattleFood(ItemReference.BATTLEFOOD_DEFAULT_ID, 1, 0.1F, false);
		bundledFood = new ItemBundledFood(ItemReference.BUNDLEDFOOD_DEFAULT_ID, 1, 0.1F, false);
		travelerFood = new ItemTravelerFood(ItemReference.TRAVELERFOOD_DEFAULT_ID, 1, 0.1F, false);
		climberFood = new ItemClimberFood(ItemReference.CLIMBERFOOD_DEFAULT_ID, 1, 0.1F, false);
		minerFood = new ItemMinerFood(ItemReference.MINERFOOD_DEFAULT_ID, 1, 0.1F, false);
		cookedFlesh = new ItemCookedZombieFlesh(ItemReference.COOKED_ZOMBIE_DEFAULT_ID, 4, 1.0F, false);
		PowerFoodMod.opBundledFood = new ItemOPBundledFood(ItemReference.OP_BUNDLEDFOOD_DEFAULT_ID, 1, 0.1F, false);

	}

	public void addNames() {
		mlr.nameObject(basicFood, ItemReference.BASICFOOD_COOKED_NAME_EN, ItemReference.BASICFOOD_COOKED_NAME_DE);
		mlr.nameObject(basicFoodCooked, ItemReference.BASICFOOD_COOKED_NAME_EN, ItemReference.BASICFOOD_COOKED_NAME_DE);
		mlr.nameObject(battleFood, ItemReference.BATTLEFOOD_NAME_EN, ItemReference.BATTLEFOOD_NAME_DE);
		mlr.nameObject(bundledFood, ItemReference.BUNDLEDFOOD_NAME_EN, ItemReference.BUNDLEDFOOD_NAME_DE);
		mlr.nameObject(travelerFood, ItemReference.TRAVELERFOOD_NAME_EN, ItemReference.TRAVELERFOOD_NAME_DE);
		mlr.nameObject(climberFood, ItemReference.CLIMBERFOOD_NAME_EN, ItemReference.CLIMBERFOOD_NAME_DE);
		mlr.nameObject(minerFood, ItemReference.MINERFOOD_NAME_EN, ItemReference.MINERFOOD_NAME_DE);
		mlr.nameObject(cookedFlesh, ItemReference.COOKED_ZOMBIE_NAME_EN, ItemReference.COOKED_ZOMBIE_NAME_DE);
	}

	public void addRecipes() {
		CraftingManager.getInstance().addRecipe(new ItemStack(basicFood, 3), new Object[] { " S ", "PBC", " S ", 'S', Item.sugar, 'P', Item.porkRaw, 'B', Item.beefRaw, 'C', Item.chickenRaw });
		CraftingManager.getInstance().addRecipe(new ItemStack(basicFoodCooked, 3), new Object[] { " S ", "PBC", " S ", 'S', Item.sugar, 'P', Item.porkCooked, 'B', Item.beefCooked, 'C', Item.chickenCooked });
		FurnaceRecipes.smelting().addSmelting(856, new ItemStack(basicFoodCooked), 100);
		CraftingManager.getInstance().addRecipe(new ItemStack(battleFood, 4), new Object[] { "SRS", "GWG", "SRS", 'S', Item.sugar, 'W', Item.swordWood, 'G', Item.glowstone, 'R', Item.redstone });
		CraftingManager.getInstance().addShapelessRecipe(new ItemStack(bundledFood, 4), new ItemStack(travelerFood), new ItemStack(battleFood), new ItemStack(minerFood), new ItemStack(climberFood));
		CraftingManager.getInstance().addRecipe(new ItemStack(travelerFood, 4), new Object[] { "SGS", "WSW", "SGS", 'G', Item.glowstone, 'S', Item.sugar, 'W', Item.wheat });
		CraftingManager.getInstance().addRecipe(new ItemStack(minerFood, 4), new Object[] { "SRS", "GWG", "SRS", 'S', Item.sugar, 'W', Item.pickaxeWood, 'G', Item.glowstone, 'R', Item.redstone });
		FurnaceRecipes.smelting().addSmelting(Item.rottenFlesh.itemID, new ItemStack(cookedFlesh), 100);

		CraftingManager.getInstance().addRecipe(new ItemStack(Item.feather, 3), new Object[] { "STS", "STS", "STS", 'S', Item.silk, 'T', Item.stick });
		CraftingManager.getInstance().addRecipe(new ItemStack(Item.feather, 2), new Object[] { "STS", "STS", "   ", 'S', Item.silk, 'T', Item.stick });
		CraftingManager.getInstance().addRecipe(new ItemStack(Item.feather, 1), new Object[] { "STS", "   ", "   ", 'S', Item.silk, 'T', Item.stick });
	}
}
