package ch.jmnetwork.powerfood.items;

public class ItemReference {
	//@formatter:off
	/*
	 public static final String _UNLOCALIZED_NAME ="";
	 public static final String _NAME_EN ="";
	 public static final String _NAME_DE ="";
	 public static final int _DEFAULT_ID =0;
	 public static final String _ICON ="";
	 */
	//@formatter:on

	public static final String TEXTURE_LOC = "pwrfood";

	public static final String BASICFOOD_UNLOCALIZED_NAME = "basicFood";
	public static final String BASICFOOD_NAME_EN = "Basic Food";
	public static final String BASICFOOD_NAME_DE = "Einfaches Essen";
	public static final int BASICFOOD_DEFAULT_ID = 600;
	public static final String BASICFOOD_ICON = "basicFood";

	public static final String BASICFOOD_COOKED_UNLOCALIZED_NAME = "basicFoodCooked";
	public static final String BASICFOOD_COOKED_NAME_EN = "Cooked Basic Food";
	public static final String BASICFOOD_COOKED_NAME_DE = "Gekochtes Einfaches Essen";
	public static final int BASICFOOD_COOKED_DEFAULT_ID = 601;
	public static final String BASICFOOD_COOKED_ICON = "basicFoodCooked";

	public static final String TRAVELERFOOD_UNLOCALIZED_NAME = "travelerFood";
	public static final String TRAVELERFOOD_NAME_EN = "Traveler Food";
	public static final String TRAVELERFOOD_NAME_DE = "Essen des Wandernden";
	public static final int TRAVELERFOOD_DEFAULT_ID = 602;

	public static final String CLIMBERFOOD_UNLOCALIZED_NAME = "climberFood";
	public static final String CLIMBERFOOD_NAME_EN = "Climber Food";
	public static final String CLIMBERFOOD_NAME_DE = "Essen des Bergsteigers";
	public static final int CLIMBERFOOD_DEFAULT_ID = 603;
	public static final String CLIMBERFOOD_ICON = "climberFood";

	public static final String COOKED_ZOMBIE_UNLOCALIZED_NAME = "cookedFlesh";
	public static final String COOKED_ZOMBIE_NAME_EN = "Cooked Zombie Flesh";
	public static final String COOKED_ZOMBIE_NAME_DE = "Gekochtes Zombiefleisch";
	public static final int COOKED_ZOMBIE_DEFAULT_ID = 604;
	public static final String COOKED_ZOMBIE_ICON = "cookedFlesh";

	public static final String TRAVELERFOOD_ICON = "travelerFood";
	public static final String BATTLEFOOD_UNLOCALIZED_NAME = "battleFood";
	public static final String BATTLEFOOD_NAME_EN = "Battle Food";
	public static final String BATTLEFOOD_NAME_DE = "Essen des Kaempfers";
	public static final int BATTLEFOOD_DEFAULT_ID = 605;
	public static final String BATTLEFOOD_ICON = "battleFood";

	public static final String BUNDLEDFOOD_UNLOCALIZED_NAME = "bundledFood";
	public static final String BUNDLEDFOOD_NAME_EN = "Bundled Food";
	public static final String BUNDLEDFOOD_NAME_DE = "Gebuendeltes Essen";
	public static final int BUNDLEDFOOD_DEFAULT_ID = 607;
	public static final String BUNDLEDFOOD_ICON = "bundledFood";

	public static final String MINERFOOD_UNLOCALIZED_NAME = "minerFood";
	public static final String MINERFOOD_NAME_EN = "Miner Food";
	public static final String MINERFOOD_NAME_DE = "Essen des Miners";
	public static final int MINERFOOD_DEFAULT_ID = 606;
	public static final String MINERFOOD_ICON = "minerFood";

	public static final String OP_BUNDLEDFOOD_UNLOCALIZED_NAME = "opBundledFood";
	public static final String OP_BUNDLEDFOOD_NAME_EN = "OP Bundled Food";
	public static final String OP_BUNDLEDFOOD_NAME_DE = "OP Bundled Food";
	public static final int OP_BUNDLEDFOOD_DEFAULT_ID = 608;
	public static final String OP_BUNDLEDFOOD_ICON = "opBundledFood";

}
