package ch.jmnetwork.powerfood.food;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import ch.jmnetwork.powerfood.items.ItemReference;
import ch.jmnetwork.powerfood.lib.Reference;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.ItemFood;

public class ItemBasicFoodCooked extends ItemFood {

	public ItemBasicFoodCooked(int par1, int par2, float par3, boolean par4, int par5, int par6, int par7, float par8) {
		super(par1, par2, par3, par4);
		this.setPotionEffect(par5, par6, par7, par8);
		this.setAlwaysEdible();
		setUnlocalizedName(ItemReference.BASICFOOD_COOKED_UNLOCALIZED_NAME);
		setCreativeTab(Reference.CREATIVE_TAB_FOOD);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {
		itemIcon = register.registerIcon(Reference.jutil.getTextureString(ItemReference.BASICFOOD_COOKED_ICON));
	}

}
