package ch.jmnetwork.powerfood.food;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import ch.jmnetwork.powerfood.items.ItemReference;
import ch.jmnetwork.powerfood.lib.Reference;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.ItemFood;

public class ItemCookedZombieFlesh extends ItemFood {

	public ItemCookedZombieFlesh(int par1, int par2, float par3, boolean par4) {
		super(par1, par2, par3, par4);
		setPotionEffect(17, 10, 4, 0.2F);
		setUnlocalizedName(ItemReference.COOKED_ZOMBIE_UNLOCALIZED_NAME);
		setCreativeTab(Reference.CREATIVE_TAB_FOOD);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {
		itemIcon = register.registerIcon(Reference.jutil.getTextureString(ItemReference.COOKED_ZOMBIE_ICON));
	}
}
