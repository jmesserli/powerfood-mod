package ch.jmnetwork.powerfood.food;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import ch.jmnetwork.powerfood.items.ItemReference;
import ch.jmnetwork.powerfood.lib.Reference;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;

public class ItemTravelerFood extends ItemFood {

	public ItemTravelerFood(int par1, int par2, float par3, boolean par4) {
		super(par1, par2, par3, par4);
		this.setPotionEffect(1, 60, 4, 1.0F);
		this.setAlwaysEdible();
		setUnlocalizedName(ItemReference.TRAVELERFOOD_UNLOCALIZED_NAME);
		setCreativeTab(Reference.CREATIVE_TAB_FOOD);
	}

	public int getMaxItemUseDuration(ItemStack par1ItemStack) {
		return Reference.EFFECT_ONLY_EAT_TIME;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {
		itemIcon = register.registerIcon(Reference.jutil.getTextureString(ItemReference.TRAVELERFOOD_ICON));
	}
}
