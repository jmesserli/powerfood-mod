package ch.jmnetwork.powerfood.food;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import ch.jmnetwork.powerfood.items.ItemReference;
import ch.jmnetwork.powerfood.lib.Reference;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class ItemBundledFood extends ItemFood {

	public ItemBundledFood(int par1, int par2, float par3, boolean par4) {
		super(par1, par2, par3, par4);
		setAlwaysEdible();
		setUnlocalizedName(ItemReference.BUNDLEDFOOD_UNLOCALIZED_NAME);
		setCreativeTab(Reference.CREATIVE_TAB_FOOD);
	}

	public int getMaxItemUseDuration(ItemStack par1ItemStack) {
		return Reference.EFFECT_ONLY_EAT_TIME;
	}

	public ItemStack onEaten(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer) {
		--par1ItemStack.stackSize;
		par3EntityPlayer.getFoodStats().addStats(this);
		par2World.playSoundAtEntity(par3EntityPlayer, "random.burp", 0.5F, par2World.rand.nextFloat() * 0.1F + 0.9F);
		/* Haste 3 / Miner Food */par3EntityPlayer.addPotionEffect(new PotionEffect(3, 60 * 20, 2));
		/* Strength 3 / Battle Food */par3EntityPlayer.addPotionEffect(new PotionEffect(5, 60 * 20, 2));
		/* Jump Boost 4 / Climber Food */par3EntityPlayer.addPotionEffect(new PotionEffect(8, 60 * 20, 3));
		/* Swiftness 5 / Traveler Food */par3EntityPlayer.addPotionEffect(new PotionEffect(1, 60 * 20, 4));
		this.onFoodEaten(par1ItemStack, par2World, par3EntityPlayer);
		return par1ItemStack;
	}

	public boolean hasEffect(ItemStack par1ItemStack) {
		return true;
	}

	@SideOnly(Side.CLIENT)
	public EnumRarity getRarity(ItemStack par1ItemStack) {
		return EnumRarity.uncommon;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {
		itemIcon = register.registerIcon(Reference.jutil.getTextureString(ItemReference.BUNDLEDFOOD_ICON));
	}
}
