package ch.jmnetwork.powerfood.food;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import ch.jmnetwork.powerfood.items.ItemReference;
import ch.jmnetwork.powerfood.lib.Reference;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;

public class ItemBattleFood extends ItemFood {

	public ItemBattleFood(int par1, int par2, float par3, boolean par4) {
		super(par1, par2, par3, par4);
		setPotionEffect(5, 60, 2, 1.0F);
		setAlwaysEdible();
		setUnlocalizedName(ItemReference.BATTLEFOOD_UNLOCALIZED_NAME);
		setCreativeTab(Reference.CREATIVE_TAB_FOOD);
	}

	public int getMaxItemUseDuration(ItemStack par1ItemStack) {
		return Reference.EFFECT_ONLY_EAT_TIME;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {
		itemIcon = register.registerIcon(Reference.jutil.getTextureString(ItemReference.BATTLEFOOD_ICON));
	}
}
