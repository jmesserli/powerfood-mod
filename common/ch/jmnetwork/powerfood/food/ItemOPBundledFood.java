package ch.jmnetwork.powerfood.food;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import ch.jmnetwork.powerfood.items.ItemReference;
import ch.jmnetwork.powerfood.lib.Reference;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemOPBundledFood extends ItemFood {
	public ItemOPBundledFood(int par1, int par2, float par3, boolean par4) {
		super(par1, par2, par3, par4);
		setAlwaysEdible();
		setUnlocalizedName(ItemReference.OP_BUNDLEDFOOD_UNLOCALIZED_NAME);
	}

	public int getMaxItemUseDuration(ItemStack par1ItemStack) {
		return Reference.EFFECT_ONLY_EAT_TIME;
	}

	public ItemStack onEaten(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer) {
		--par1ItemStack.stackSize;
		par3EntityPlayer.getFoodStats().addStats(this);
		par2World.playSoundAtEntity(par3EntityPlayer, "random.burp", 0.5F, par2World.rand.nextFloat() * 0.1F + 0.9F);
		/* Haste 10 / Miner Food */par3EntityPlayer.addPotionEffect(new PotionEffect(3, 60 * 20, 9));
		/* Strength 10 / Battle Food */par3EntityPlayer.addPotionEffect(new PotionEffect(5, 60 * 20, 9));
		/* Jump Boost 3 / Climber Food */par3EntityPlayer.addPotionEffect(new PotionEffect(8, 60 * 20, 2));
		/* Swiftness 10 / Traveler Food */par3EntityPlayer.addPotionEffect(new PotionEffect(1, 60 * 20, 9));
		this.onFoodEaten(par1ItemStack, par2World, par3EntityPlayer);
		return par1ItemStack;
	}

	public boolean hasEffect(ItemStack par1ItemStack) {
		return true;
	}

	@SideOnly(Side.CLIENT)
	public EnumRarity getRarity(ItemStack par1ItemStack) {
		return EnumRarity.epic;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {
		itemIcon = register.registerIcon(Reference.jutil.getTextureString(ItemReference.OP_BUNDLEDFOOD_ICON));
	}
}
