package ch.jmnetwork.powerfood.food;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import ch.jmnetwork.powerfood.lib.Reference;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemBundledFoodTextureOnly extends Item {

	public ItemBundledFoodTextureOnly(int par1) {
		super(par1);
		setUnlocalizedName("bundledFoodTexture");
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {
		itemIcon = register.registerIcon(Reference.jutil.getTextureString("bundledFoodTexture"));
	}

}
